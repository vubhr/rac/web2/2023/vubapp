<?php


if (isset($GLOBALS['language'])){
    switch ($GLOBALS['language']){
        case 'ENG':
            $GLOBALS['login_err']     = json_decode('{"h_message":"Please log in to the application","h_errcode":999}', false);
            $GLOBALS['request_err']   = json_decode('{"h_message":"Invalid request type","h_errcode":998}', false);
            $GLOBALS['procedure_err'] = json_decode('{"h_message":"Unknown procedure","h_errcode":997}', false);
            $GLOBALS['logout']        = json_decode('{"h_message":"You have successfully logged out","h_errcode":0}', false);
            break;
       default:
            
            break;
    }
}else{
    $GLOBALS['login_err']     = json_decode('{"h_message":"Molimo prijavite se na aplikaciju","h_errcode":999}', false);
    $GLOBALS['request_err']   = json_decode('{"h_message":"Neispravna vrsta zahtjeva","h_errcode":998}', false);
    $GLOBALS['procedure_err'] = json_decode('{"h_message":"Nepoznata procedura","h_errcode":997}', false);
    $GLOBALS['login_OK']      = json_decode('{"h_message":"Login OK","h_errcode":996}', false);
    $GLOBALS['login_NOTOK']   = json_decode('{"h_message":"Neispravno korisničko ime ili zaporka","h_errcode":995}', false);
    $GLOBALS['logout']        = json_decode('{"h_message":"Uspješno ste odjavljeni","h_errcode":0}', false);
    $GLOBALS['register']      = json_decode('{"h_message":"Uspješno ste se registrirali","h_errcode":0}', false);
}

?>