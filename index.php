<?php
include('./messages.php');
include('./credentials.php');
include('./authorization.php');
include('./getdata.php');
include('./postdata.php');
include('./putdata.php');
include('./deletedata.php');


$allowedOrigins = [
	'http://localhost',
	'http://localhost:5500',
	'https://uat.vub.zone'
];


$allowedProcedure = ["lokacije", "register"];

if (isset($_SERVER['HTTORIGIN'])) {
    if(in_array($_SERVER['HTTORIGIN'], $allowedOrigins)){
        $httorigin = $_SERVER['HTTORIGIN'];
     } else {
        $httorigin = "https://example.com";
     }
    }else{
        $httorigin = "https://example.com";
}

header("Access-Control-Allow-Origin: $httorigin");

header('Access-Control-Allow-Credentials: true');

session_set_cookie_params(['samesite' => 'None', 'secure' => true]);

session_start();

header('Set-Cookie: ' . session_name() . '=' . session_id() . '; path=/; HttpOnly; SameSite=Lax');

//dohvaćam ulazni json string i pretvaram ga u stdClass Objekt
$in_obj = json_decode(file_get_contents("php://input"), false);


//login
if($in_obj->procedura == "login"){
   $out_obj = checkLogin($in_obj);
   echo getOutput($out_obj);
   return; 
}

//refresh vraća podatke iz session-a
if ($in_obj->procedura == "refresh") {
    echo getOutput($_SESSION);
    return; 
}

//logout
if($in_obj->procedura =="logout"){
    session_destroy();
    echo getOutput($GLOBALS['logout'] );
    return; 
}

//dopuštene procedure bez logina
if (!in_array($in_obj->procedura, $allowedProcedure)) {
    if (!isset( $_SESSION['ID']) && $in_obj->procedura !="login") {
        echo getOutput($GLOBALS['login_err'] );
        return; 
    }
 }

//dodajem session id u ulazni objekt
$in_obj->session_id = session_id();

//ako je user postavljen u sessionu tada ga ubacujem u input objekt
if (isset($_SESSION['ID'])){
   $in_obj->UserID = $_SESSION['ID'];
}

switch ($_SERVER['REQUEST_METHOD']){
    case 'GET':
        $out_obj = getData($in_obj);
        break;
    case 'POST':
        $out_obj = postData($in_obj);
        break;
   case 'PUT':
        $out_obj = putData($in_obj);
        break;
   case 'DELETE':
        $out_obj = deleteData($in_obj);    
        break;
   default:
        $out_obj = $GLOBALS['request_err'] ;
        break;
}

$out_obj->method = $_SERVER['REQUEST_METHOD'];

//ispis na servis
echo getOutput($out_obj);



//formiranje ouputa
function getOutput($out_obj){
   // Postavljanje zaglavlja Content-Type na application/json
   header('Content-Type: application/json');

   //Pretvorba stdClass objekt u JSON string
   echo json_encode($out_obj);
}
?>