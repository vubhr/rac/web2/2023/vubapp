<?php
function putData($in_obj)
{

    switch ($in_obj->procedura) {
        case 'register':
            $out_obj = f_checkRegister($in_obj);
            if ($out_obj) {
                $out_obj = setRegister($in_obj);
            }
            break;
        default:
            $out_obj = $GLOBALS['procedure_err'];
            break;
    }
    return $out_obj;
}


function f_checkRegister($in_obj)
{
    return true;
}


function setRegister($in_obj)
{
    $pdo = getDSN();

    $sql = "INSERT INTO public.a_users (first_name, last_name, email, password, type, date_of_birth, suffix, prefix) values 
    (:first_name, :last_name, :email, :password, :type, :date_of_birth, :suffix, :prefix)";
    
    $stmt = $pdo->prepare($sql);
    $stmt->execute([
        'first_name' => $in_obj->first_name, 'last_name' => $in_obj->last_name,
        'email' => $in_obj->email, 'password' => $in_obj->password, 'type' => $in_obj->type,
        'date_of_birth' => $in_obj->date_of_birth,
        'suffix' => $in_obj->suffix,
        'prefix' => $in_obj->prefix

    ]);
    
    return $GLOBALS['register'];
}
