drop table a_verificators;
drop table a_documents;
drop table a_verificator_templates;
drop table a_document_templates;
drop table a_roles_details;
drop table a_application_roles;
drop table a_applications;
drop table a_roles;
drop table a_enrollments;
drop table a_course_professors;
drop table a_methods;
drop table a_users; 
drop table a_courses

CREATE TABLE public.a_users (
	id serial4 NOT NULL,
	first_name varchar(60) NOT NULL,
	last_name varchar(60) NOT NULL,
	email varchar(60) NOT NULL,
	password varchar(60) NOT NULL,
	type int4 NOT NULL,
	date_of_birth DATE NOT NULL,
	suffix varchar(30) NOT NULL,
	prefix varchar(30) NOT NULL,
	CONSTRAINT users_pk PRIMARY KEY (id)
);

CREATE TABLE public.a_courses (
	id serial NOT NULL,
	name varchar(60) NOT NULL,
	status varchar(30) NOT NULL,
	semester int4 NOT NULL,
	ects int4 NOT NULL,
	CONSTRAINT courses_pk PRIMARY KEY (id)
);

CREATE TABLE public.a_enrollments (
	id serial NOT NULL,
	idcourse int4 NOT NULL,
	iduser int4 NOT NULL,
	team int4 NOT NULL,
	CONSTRAINT enrollments_pk PRIMARY KEY (id)
);



CREATE TABLE public.a_document_templates (
	id serial4 NOT NULL,
	name varchar(255) NOT NULL,
	type int4 NOT NULL,
	body varchar(4000) NOT NULL,
	CONSTRAINT document_templates_pk PRIMARY KEY (id)
);



CREATE TABLE public.a_documents (
	id serial4 NOT NULL,
	iduser int4 NOT NULL,
	iddocument_template int4 NOT NULL,
	name varchar(255) NOT NULL,
	status int4 NOT NULL,
	body varchar(4000) NOT NULL,
	CONSTRAINT documents_pk PRIMARY KEY (id)
);



CREATE TABLE public.a_roles (
	id serial4 NOT NULL,
	name varchar(255) NOT NULL,
	type int4 NOT NULL,
	CONSTRAINT roles_pk PRIMARY KEY (id)
);



CREATE TABLE public.a_roles_details (
	id serial4 NOT NULL,
	iduser int4 NOT NULL,
	idrole int4 NOT NULL,
	valid_from DATE NOT NULL,
	valid_until DATE NOT NULL,
	get int4 NOT NULL,
	put int4 NOT NULL,
	post int4 NOT NULL,
	del int4 NOT NULL,
	verificator int4 NOT NULL,
	CONSTRAINT roles_details_pk PRIMARY KEY (id)
);



CREATE TABLE public.a_applications (
	id serial4 NOT NULL,
	route varchar(255) NOT NULL,
	name varchar(60) NOT NULL,
	description varchar(1000) NOT NULL,
	CONSTRAINT applications_pk PRIMARY KEY (id)
);



CREATE TABLE public.a_application_roles (
	id serial4 NOT NULL,
	idrole int4 NOT NULL,
	idapplication int4 NOT NULL,
	CONSTRAINT application_roles_pk PRIMARY KEY (id)
);



CREATE TABLE public.a_verificator_templates (
	id serial4 NOT NULL,
	iddocument_template int4 NOT NULL,
	idrole int4 NOT NULL,
	CONSTRAINT verificator_templates_pk PRIMARY KEY (id)
);



CREATE TABLE public.a_verificators (
	id serial4 NOT NULL,
	iddocument int4 NOT NULL,
	idrole int4 NOT NULL,
	CONSTRAINT verificators_pk PRIMARY KEY (id)
);

CREATE TABLE public.a_methods (
	id serial NOT NULL,
	idcourse int4 NOT NULL,
	type int4 NOT NULL,
	hours int4 NOT NULL,
	weekly int4 NOT NULL,
	team int4 NOT NULL,
	CONSTRAINT methods_pk PRIMARY KEY (id)
);

CREATE TABLE public.a_course_professors (
	id serial NOT NULL,
	idmethod int4 NOT NULL,
	iduser int4 NOT NULL,
	percentage DECIMAL(5,2) NOT NULL,
	coordinator int4 NOT NULL,
	CONSTRAINT course_professors_pk PRIMARY KEY (id)
);

CREATE TABLE public.a_classrooms (
	id serial NOT NULL,
	name varchar(60) NOT NULL,
	is_lab integer NOT NULL,
	location varchar(255) NOT NULL,
	capacity integer NOT NULL,
	squares DECIMAL(4,2) NOT NULL,
	CONSTRAINT classrooms_pk PRIMARY KEY (id)
);


ALTER TABLE a_documents ADD CONSTRAINT a_documents_fk0 FOREIGN KEY (iduser) REFERENCES a_users(id);
ALTER TABLE a_documents ADD CONSTRAINT a_documents_fk1 FOREIGN KEY (iddocument_template) REFERENCES a_document_templates(id);


ALTER TABLE a_roles_details ADD CONSTRAINT a_roles_details_fk0 FOREIGN KEY (iduser) REFERENCES a_users(id);
ALTER TABLE a_roles_details ADD CONSTRAINT  a_roles_details_fk1 FOREIGN KEY (idrole) REFERENCES a_roles(id);


ALTER TABLE a_application_roles ADD CONSTRAINT a_application_roles_fk0 FOREIGN KEY (idrole) REFERENCES a_roles(id);
ALTER TABLE a_application_roles ADD CONSTRAINT a_application_roles_fk1 FOREIGN KEY (idapplication) REFERENCES a_applications(id);

ALTER TABLE a_verificator_templates ADD CONSTRAINT a_verificator_templates_fk0 FOREIGN KEY (iddocument_template) REFERENCES a_document_templates(id);
ALTER TABLE a_verificator_templates ADD CONSTRAINT a_verificator_templates_fk1 FOREIGN KEY (idrole) REFERENCES a_roles(id);

ALTER TABLE a_verificators ADD CONSTRAINT a_verificators_fk0 FOREIGN KEY (iddocument) REFERENCES a_documents(id);
ALTER TABLE a_verificators ADD CONSTRAINT a_verificators_fk1 FOREIGN KEY (idrole) REFERENCES a_roles(id);

ALTER TABLE a_enrollments ADD CONSTRAINT a_enrollments_fk0 FOREIGN KEY (idcourse) REFERENCES a_courses(id);
ALTER TABLE a_enrollments ADD CONSTRAINT a_enrollments_fk1 FOREIGN KEY (iduser) REFERENCES a_users(id);

ALTER TABLE a_methods ADD CONSTRAINT a_methods_fk0 FOREIGN KEY (idcourse) REFERENCES a_courses(id);

ALTER TABLE a_course_professors ADD CONSTRAINT a_course_professors_fk0 FOREIGN KEY (idmethod) REFERENCES a_methods(id);
ALTER TABLE a_course_professors ADD CONSTRAINT a_course_professors_fk1 FOREIGN KEY (iduser) REFERENCES a_users(id);

-- Ubacivanje dummy podataka u tablicu a_users
INSERT INTO public.a_users (first_name, last_name, email, password, type, date_of_birth, suffix, prefix) VALUES
('Marko', 'Marković', 'marko@example.com', 'lozinka1', 0, '1990-01-01', 'Mr.', ''),
('Ivana', 'Ivanović', 'ivana@example.com', 'lozinka2', 1, '1991-02-02', 'Ms.', ''),
('Petar', 'Petrović', 'petar@example.com', 'lozinka3', 0, '1992-03-03', 'Mr.', ''),
('Ana', 'Anić', 'ana@example.com', 'lozinka4', 1, '1993-04-04', 'Ms.', ''),
('Tomislav', 'Tomić', 'tomislav@example.com', 'lozinka5', 0, '1994-05-05', 'Mr.', ''),
('Marija', 'Marić', 'marija@example.com', 'lozinka6', 1, '1995-06-06', 'Ms.', ''),
('Luka', 'Lukić', 'luka@example.com', 'lozinka7', 0, '1996-07-07', 'Mr.', ''),
('Martina', 'Martinović', 'martina@example.com', 'lozinka8', 1, '1997-08-08', 'Ms.', ''),
('Igor', 'Igorić', 'igor@example.com', 'lozinka9', 0, '1998-09-09', 'Mr.', ''),
('Katarina', 'Katić', 'katarina@example.com', 'lozinka10', 1, '1999-10-10', 'Ms.', ''),
('Josip', 'Josipović', 'josip@example.com', 'lozinka11', 0, '2000-11-11', 'Mr.', ''),
('Maja', 'Majić', 'maja@example.com', 'lozinka12', 1, '2001-12-12', 'Ms.', ''),
('Tihomir', 'Tihomirović', 'tihomir@example.com', 'lozinka13', 2, '1992-03-03', 'Mr.', '');

-- Ubacivanje dummy podataka u tablicu a_document_templates
INSERT INTO public.a_document_templates (name, type, body) VALUES
('Template 1', 1, 'Ovo je tijelo dokumenta 1.'),
('Template 2', 2, 'Ovo je tijelo dokumenta 2.');

-- Ubacivanje dummy podataka u tablicu a_documents
INSERT INTO public.a_documents (iduser, iddocument_template, name, status, body) VALUES
(1, 1, 'Dokument 1', 1, 'Ovo je tijelo dokumenta 1 za korisnika 1.'),
(2, 2, 'Dokument 2', 2, 'Ovo je tijelo dokumenta 2 za korisnika 2.');

-- Ubacivanje dummy podataka u tablicu a_roles
INSERT INTO public.a_roles (name, type) VALUES
('Administrator', 1),
('Korisnik', 2);

-- Ubacivanje dummy podataka u tablicu a_roles_details
INSERT INTO public.a_roles_details (iduser, idrole, valid_from, valid_until, get, put, post, del, verificator) VALUES
(1, 1, '2023-01-01', '2023-12-31', 1, 1, 1, 1, 1),
(2, 2, '2023-01-01', '2023-12-31', 1, 0, 0, 0, 0);

-- Ubacivanje dummy podataka u tablicu a_applications
INSERT INTO public.a_applications (route, name, description) VALUES
('/app1', 'Aplikacija 1', 'Ovo je opis aplikacije 1. Administratorska aplikacija'),
('/app2', 'Aplikacija 2', 'Ovo je opis aplikacije 2. Korisnička aplikacija');


-- Ubacivanje dummy podataka u tablicu a_application_roles
INSERT INTO public.a_application_roles (idrole, idapplication) VALUES
(1, 1),
(2, 2);

-- Ubacivanje dummy podataka u tablicu a_verificator_templates
INSERT INTO public.a_verificator_templates (iddocument_template, idrole) VALUES
(1, 1),
(2, 2);

-- Ubacivanje dummy podataka u tablicu a_verificators
INSERT INTO public.a_verificators (iddocument, idrole) VALUES
(1, 1),
(2, 2);
  

INSERT INTO public.a_application_roles (idrole, idapplication) VALUES
(2, 1);


INSERT INTO public.a_users (first_name, last_name, email, password, type, date_of_birth, suffix, prefix) VALUES
('Petar', 'Petrović', 'petar@example.com', 'lozinka3', 0, '1992-03-03', 'Mr.', ''),
('Ana', 'Anić', 'ana@example.com', 'lozinka4', 1, '1993-04-04', 'Ms.', ''),
('Tomislav', 'Tomić', 'tomislav@example.com', 'lozinka5', 0, '1994-05-05', 'Mr.', ''),
('Marija', 'Marić', 'marija@example.com', 'lozinka6', 1, '1995-06-06', 'Ms.', ''),
('Luka', 'Lukić', 'luka@example.com', 'lozinka7', 0, '1996-07-07', 'Mr.', ''),
('Martina', 'Martinović', 'martina@example.com', 'lozinka8', 1, '1997-08-08', 'Ms.', ''),
('Igor', 'Igorić', 'igor@example.com', 'lozinka9', 0, '1998-09-09', 'Mr.', ''),
('Katarina', 'Katić', 'katarina@example.com', 'lozinka10', 1, '1999-10-10', 'Ms.', ''),
('Josip', 'Josipović', 'josip@example.com', 'lozinka11', 0, '2000-11-11', 'Mr.', ''),
('Maja', 'Majić', 'maja@example.com', 'lozinka12', 1, '2001-12-12', 'Ms.', ''),
('Tihomir', 'Tihomirović', 'tihomir@example.com', 'lozinka13', 2, '1992-03-03', 'Mr.', '');



INSERT INTO public.a_courses (name, status, semester, ects) VALUES
('Anatomija', 1, 1, 3),
('Uvod u zdravstvenu njegu', 1, 2, 3),
('Komunikacijske vještine', 1, 1, 3),
('Osnove elektrotehnike', 1, 1, 7),
('Tehnički materijali', 1, 1, 5),
('Matematika 1', 1, 1, 7),
('Uvod u programiranje', 1, 1, 6),
('Baze podataka', 1, 3, 6),
('Programski jezik C', 1, 2, 7),
('Mikroračunala', 1, 4, 4);

--type
/*
 0 predavanje
 1 Auditorne vježbe
 2 Laboratorijske vježbe
 3 Seminar
 4 M
 5 PRK
 6 PK
 7 T
 8 KL
 9 PKL
 
 */
INSERT INTO public.a_methods (idcourse, type, hours, weekly, team) VALUES
(1, 0, 30, 2, 1),
(1, 5, 20, 2, 4),
(2, 0, 10, 2, 1),
(2, 5, 75, 4, 4),
(3, 0, 15, 2, 1),
(3, 4, 15, 2, 1),
(4, 0, 30, 2, 1),
(4, 1, 30, 2, 1),
(4, 2, 15, 2, 2),
(5, 0, 30, 2, 1),
(5, 1, 24, 2, 1),
(5, 2, 6, 2, 2),
(6, 0, 30, 2, 1),
(6, 1, 45, 3, 1),
(7, 0, 30, 2, 1),
(7, 2, 30, 2, 2),
(8, 0, 30, 2, 1),
(8, 2, 30, 2, 2),
(9, 0, 30, 2, 1),
(9, 2, 45, 3, 2),
(10, 0, 15, 2, 1),
(10, 2, 30, 2, 2);


INSERT INTO public.a_course_professors (idmethod, iduser, percentage, coordinator) VALUES
(1, 2, 100, 1),
(2, 4, 100, 0),
(3, 6, 100, 1),
(4, 4, 100, 0),
(5, 8, 100, 1),
(6, 8, 100, 0),
(7, 10, 100, 1),
(8, 10, 100, 0),
(9, 10, 100, 0),
(10, 10, 100, 1),
(11, 10, 100, 0),
(12, 10, 100, 0),
(13, 8, 100, 1),
(14, 8, 100, 0),
(15, 12, 100, 1),
(16, 12, 100, 0),
(17, 10, 100, 1),
(18, 10, 100, 0),
(19, 10, 100, 1),
(20, 10, 100, 0),
(21, 8, 100, 1),
(22, 8, 100, 0);
