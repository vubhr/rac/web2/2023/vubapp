<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInite049906b451832e21538c73847da5880
{
    public static $prefixLengthsPsr4 = array (
        'O' => 
        array (
            'Opis\\JsonSchema\\' => 16,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Opis\\JsonSchema\\' => 
        array (
            0 => __DIR__ . '/..' . '/opis/json-schema/src',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInite049906b451832e21538c73847da5880::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInite049906b451832e21538c73847da5880::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInite049906b451832e21538c73847da5880::$classMap;

        }, null, ClassLoader::class);
    }
}
